# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Denormalizer in Spark.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

** Summary of set up on local machine

1. Clone this repo 
2. change the parameters in quintiles.properties under <root>/src/main/resources/
3. rename soleconfig.xml.template to solrconfig.xml and make necessary changes.
4. Run PullSqlServerData with VM options of -Xmx12g
   and program arguments of : <path to>/quintiles.properties 1
 
** Summary of set up on server.

The command to submit the job on cluster. To run on local vm, change accordingly

nohup ./bin/spark-submit --class PullSqlServerData  --master yarn-client 
 --num-executors 40  --driver-memory 4g --executor-memory 20g  --executor-cores 1 --files 
 ../scripts/log4j.properties  --conf spark.yarn.historyServer.address=usadc-shdsxt04:18080  
  --conf spark.eventLog.dir=hdfs:///user/webadm/spark130/event_log/ --conf spark.eventLog.enabled=true 
  --conf spark.storage.memoryFraction=0.2 --jars=hdfs:///user/webadm/solr_input_files_06_2015/conf.jar  
  /u02/app/webadm/scripts/spark_denormalizer.jar /u02/app/webadm/scripts/config.properties 40 &


1. Move all the input file to the hdfs and update the quintiles.properties
2. The last argument in the above command is number of cores to be created.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact