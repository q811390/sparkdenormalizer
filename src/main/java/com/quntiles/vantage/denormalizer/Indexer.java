package com.quntiles.vantage.denormalizer;

import com.quntiles.vantage.denormalizer.vo.ConstantEvents;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: cvora
 * Date: 2/24/14
 * Time: 11:41 AM
 * To change this template use File | Settings | File Templates.
 */
public class Indexer {
    private static Medications MEDICATIONS;
    private static ProviderData PROVIDERS;
    private static DosageFormsFilterList DOSAGEFORMS;
    private static com.quntiles.vantage.denormalizer.ICD9 ICD9;

    public static void initialize(ConstantEvents constantEvents) throws Exception {
        if(constantEvents==null) {
            MEDICATIONS = new Medications("medications.txt",null);
            PROVIDERS = new ProviderData("providers.txt",null);
            DOSAGEFORMS = new DosageFormsFilterList("MedicationDosageForm.txt",null);
            ICD9 = new com.quntiles.vantage.denormalizer.ICD9("icd9strings.txt",null);
        }else
        {
            MEDICATIONS = constantEvents.getMedications();
            PROVIDERS = constantEvents.getProviders();
            DOSAGEFORMS = constantEvents.getDosageFormsFilterList();
            ICD9 = constantEvents.getIcd9();

        }
    }
    public static Collection<String> getMedicationCategories(String medicationID) {
        return MEDICATIONS.getCategories(medicationID);
    }


    public static Map<String, ArrayList<String>> getMedicationCategoryIDs(String medicationID) {
        return MEDICATIONS.getCategoryIDs(medicationID);
    }


    public static Map<String, ArrayList<String>> getICD9CodeIDs(String icd9CodeIDs) {
        return ICD9.getICD9CodeIDs(icd9CodeIDs);
    }

    public static Collection<String> getICD9Codes(String codeID) {
        return ICD9.getICD9Codes(codeID);

    }

    public static DosageFormsFilterList getDOSAGEFORMS() {
        return DOSAGEFORMS;
    }

    public static ProviderData getPROVIDERS() {
        return PROVIDERS;
    }

}
