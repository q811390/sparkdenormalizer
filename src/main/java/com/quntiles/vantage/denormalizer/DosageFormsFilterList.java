package com.quntiles.vantage.denormalizer;

import java.io.BufferedReader;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Used to apply stored procedure business rules when looking up medications for patients.
 * If a medication doesn't have a strength, then we don't index it.
 */
public class DosageFormsFilterList implements Serializable{

    public  final String COLUMN_DELIMITER = "\\|"; //pipe character needs to be escaped
    private Map<Integer, String> filterList = new HashMap<Integer, String>();

    public DosageFormsFilterList(){}

    public DosageFormsFilterList(String filename, List<String> data) throws Exception {
        System.out.println();
        System.out.println("Loading DosageFormsFilterList.");

        if(data == null) {
            Path filePath = Paths.get(filename);
            if (!Files.exists(filePath)) {
                System.out.println("Failed to open dosage forms filter file: " + filename);
                System.out.println("Program exiting");
                System.exit(0);
            }

            BufferedReader reader = Files.newBufferedReader(filePath, Charset.defaultCharset());

            String line;
            String[] parts;
            while ((line = reader.readLine()) != null) {

                processLine(line);

            }
        }
        else{
            for (String line : data) {
                processLine(line);
            }
        }

        System.out.println("Finished.");
    }

    private void processLine(String line) {
        String[] parts;
        try {
            parts = line.split(COLUMN_DELIMITER);
            double value = 0;

            filterList.put(Integer.parseInt(parts[0]), "1");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public boolean exists(int i) {
        return filterList.containsKey(i);
    }

    public String getStrength(int i) {
        return filterList.get(i);
    }
}
