package com.quntiles.vantage.denormalizer.contracts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by q811390 on 5/11/2015.
 */
public class OutPatients implements Serializable{
    private List<OutputPatient> list = new ArrayList<>();

    public OutPatients() {
    }

    public OutPatients(List<OutputPatient> list) {

        this.list = list;
    }

    public List<OutputPatient> getList() {
        return list;
    }

    public void setList(List<OutputPatient> list) {
        this.list = list;
    }
    //case class OutPatients(list: List[OutputPatient])
}
