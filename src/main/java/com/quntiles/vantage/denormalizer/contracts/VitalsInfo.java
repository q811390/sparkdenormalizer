package com.quntiles.vantage.denormalizer.contracts;

import com.quntiles.vantage.denormalizer.records.EventType;
import org.apache.solr.common.SolrInputDocument;

import java.text.ParseException;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: mike
 * Date: 6/10/13
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class VitalsInfo extends Info {
    private static String ID_KEY = "VITAL:";
    private static int VITAL_DESCRIPTOR = 1;
    private static int VITAL_TYPE_ID = 2;
    private static int UNIT_TYPE_ID = 3;
    private static int UNIT = 4;
    private static int VALUE_N_VARCHAR = 5;
    private static int VALUE_DECIMAL = 6;
    private static int PERFORMED_DATE = 7;
    private static int DOCUMENTING_PROVIDER = 8;
    private static int MOST_FREQ_PROVIDER = 9;
    private static int PRIMARY_CARE_PROVIDER = 10;
    private static int COMPRESSED_LINE = 21;
    private boolean valid=true;

    public VitalsInfo(){}

    public boolean isValid() {
        return valid;
    }

    private Event vitalsEvent;

    public VitalsInfo(Event vitalsInfo) {
        vitalsEvent = vitalsInfo;
    }

    public long getUniqueId() {
        return Long.parseLong(vitalsEvent.getValue(0));
    }

    public String getEventDateAsString() {
        try {
            return dateOutFormat.format(dateInFormat.parse(vitalsEvent.getValue(5)));
        } catch (Exception e) {
            return "";
        }
    }

    public String getEndDateAsString() throws Exception {
        return dateOutFormat.format(dateInFormat.parse(getEventDateAsString()));  //end date is same as eventdate for now
    }

/*
    public long getEventDateAsLong() throws Exception {
        return dateOutFormat.parse(getEventDateAsString()).getTime();
    }
*/

//    public Date getEventDate() {
//        try {
//            return dateInFormat.parse(vitalsEvent.getValue(5));   //todo: see if this is same as getAttribute(0)
//        } catch (Exception e) {
//            return new Date();
//        }
//    }

    @Override
    public Date getEventDate()  {

        try {
            return dateInFormat.parse(vitalsEvent.getAttribute(PERFORMED_DATE));
        } catch (ParseException e) {
            e.printStackTrace();  //log this instead if necessary
        }
        return null;
    }

    @Override
    public Date getStartDate() throws Exception {
        return getEventDate();
    }

    @Override
    public Date getEndDate() throws Exception {
        return null;      //end dates are null for vitals, labs, diag
    }

    @Override
    public EventType getEventType() {
        return EventType.VITALS;
    }

    public String getEventId() {
        return ID_KEY + vitalsEvent.getAttribute(VITAL_DESCRIPTOR);
    }

    public String getUnit() {
        return vitalsEvent.getAttribute(UNIT);
    }

    public String getPerformedDate() {
        try {
            return dateOutFormat.format(dateInFormat.parse(vitalsEvent.getAttribute(PERFORMED_DATE)));
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public int getProviderId() {
        return Integer.parseInt(vitalsEvent.getAttribute(MOST_FREQ_PROVIDER));
    }

    @Override
    public double getValue() {
/*
        double rounded;
        double number = Double.parseDouble(vitalsEvent.getAttribute(VALUE_DECIMAL));
        number = Math.round(number * 10);
        rounded = number / 10;
        return rounded;
*/

        double number = -1;
        String vitalDescriptor = vitalsEvent.getAttribute(VITAL_DESCRIPTOR).trim();
        String valueStr = vitalsEvent.getAttribute(VALUE_DECIMAL).trim();
        try {
            if (valueStr != null && !valueStr.equals("")) {
                if (valueStr.contains(".")) {
                    String wholeNumber = valueStr.substring(0, valueStr.indexOf("."));
                    if ("BMI".equals( vitalDescriptor) )
                    {
                        char roundingDigit = valueStr.charAt(valueStr.indexOf(".") + 1);
                        switch (roundingDigit) {
                            case '0':
                            case '1':
                            case '2':
                                number = Double.parseDouble(wholeNumber);
                                break;
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                            case '7':
                                number = Double.parseDouble((wholeNumber + ".5").trim());
                                break;
                            case '8':
                            case '9':
                                number = Double.parseDouble(wholeNumber) + 1.0;
                                break;
                            default:
                                break;
                        }
                    }
                    else {  //for weight, just round down
                        number = Double.parseDouble(wholeNumber);
                    }
                } else {    //if no decimal in the incoming value, just parse it to double, no rounding needed
                    number = Double.parseDouble(valueStr);
                }


            }
        } catch (Exception e) {
            return -1;
        }
        return number;
    }

    public String getCompressedLine() {
        return vitalsEvent.getAttribute(COMPRESSED_LINE);
    }


    @Override
    public void updateDocument(SolrInputDocument doc) throws Exception {
        doc.addField("UniqueID", getUniqueId());
        doc.addField("EventID", getEventId());
        doc.addField("EventDate", getEventDateAsString());
        doc.addField("Unit", getUnit());
        doc.addField("PerformedDate", getPerformedDate());
        doc.addField("EventDateLong", getEventDateAsLong());
        double value = getValue();
        if (value != -1)
            doc.addField("Value", value);
        doc.addField("BucketID", getBuckets());
//        doc.addField("CompressedLine", getCompressedLine());

    }

    @Override
    public String toString() {
        return vitalsEvent.toString();
    }
}
