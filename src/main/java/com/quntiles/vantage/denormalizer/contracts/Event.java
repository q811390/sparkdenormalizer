package com.quntiles.vantage.denormalizer.contracts;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: mike
 * Date: 8/23/13
 * Time: 7:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class Event implements Serializable {
    public  final String COLUMN_DELIMITER = "\\|"; //pipe character needs to be escaped
    private  String[] csv;
    private int patientId = 0;

    public Event(){}

    public Event(String csvLine) {
        csv = csvLine.split(COLUMN_DELIMITER);
    }

    public String[] getCsv() {
        return csv;
    }

    public void setCsv(String[] csv) {
        this.csv = csv;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getEventId() {
        return csv[1];
    }

    public int getPatientId() {
        if (patientId == 0)
            patientId = Integer.parseInt(csv[3]);
        return patientId;
    }

    //CV: this is a workaround put in to missing end dates
    public String getEndDateIfMedication() {
            return csv[14];
    }
    @Override
    public String toString() {
        return StringUtils.join(csv, COLUMN_DELIMITER);
    }

    public String getValue(int index) {
        return csv[index];
    }

    //returns the given attribute index
    // values 0 through 5 are UniqueID,EventType, DataSourceID, PatientID, EventLevel and EventDate
    public String getAttribute(int attribute) {
        try {
            return csv[5 + attribute];
        } catch (ArrayIndexOutOfBoundsException e) {
            if (attribute < 8)   //this error shouldn't happen for attribut 1-7
                System.out.println("Error processing data: column " + attribute + " not found for EventId" +  csv[0] + " Eventtype " + csv[1]);
            return "";
        }
        // return csv[6 + attribute]; - CV: Mike code has this. This will allow us to use * in SQL for collecting events.
        // Its not recommended to use this way of getting attributes
    }
}
