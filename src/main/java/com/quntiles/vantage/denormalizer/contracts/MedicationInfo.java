package com.quntiles.vantage.denormalizer.contracts;

import com.quntiles.vantage.denormalizer.Indexer;
import com.quntiles.vantage.denormalizer.records.EventType;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.solr.common.SolrInputDocument;
import org.joda.time.DateTime;
import org.joda.time.Days;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: mike
 * Date: 6/10/13
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class MedicationInfo extends Info {
    private final String ID_KEY = "MED:";
    private final int MEDICATION_ID = 1;
    private final int MEDICATION_NAME = 2;
    private final int STATUS = 3;
    private final int QUANTITY = 4;
    private final int DAY_SUPPLY = 5;
    private final int FREQ_PER_DAY = 6;
    private final int DOSE = 7;
    private final int START_DATE = 8;
    private final int END_DATE = 9;
    private final int ROUTE_CODE = 10;
    private final int DOSAGE_FORM_CODE = 11;
    private final int DOCUMENTING_PROVIDER = 12;
    private final int DDI = 13;
    private final int PRIMARY_CARE_PROVIDER = 14;
    private final int MOST_FREQ_PROVIDER = 15;
    private final int REFILLS = 16;
    private final int STRENGTH = 17;
    private final int UNIT= 18;
    private final int TDD= 19;
    private final int COMPRESSED_LINE = 21;
    private boolean valid = true;
    private Event medicationEvent;

    public MedicationInfo(){}

    public MedicationInfo(Event medicationInfo)  {
        medicationEvent = medicationInfo;//'', 'Unauthorized', 'Voided'

        try {


            Integer testInt = NumberUtils.createInteger(medicationEvent.getAttribute(DDI));
            if (testInt == null) {
                valid = false;
                return;
            }
            if (!Indexer.getDOSAGEFORMS().exists(testInt)) {
                valid = false;
                return;
            }
        } catch (Exception e) {
            valid = false;

        }

    }

    public boolean isValid() {
        return valid;
    }

    public long getUniqueId() {
        return Long.parseLong(medicationEvent.getValue(0));
    }

    public String getEventDateAsString()  {
        try {
            return dateOutFormat.format(dateInFormat.parse(medicationEvent.getAttribute(START_DATE)));
        } catch (ParseException e) {
            valid=false;
        }
        return null;
    }

    @Override
    public Date getEventDate() {
        try {
            return dateInFormat.parse(medicationEvent.getAttribute(START_DATE));
        } catch (ParseException e) {
        valid=true;
        }
        return null;

    }

    @Override
    public Date getStartDate()  {
        return getEventDate();
    }


    @Override
    public Date getEndDate() {
        try {
            String dateStr = medicationEvent.getAttribute(END_DATE);
            if (dateStr != null && !dateStr.equals(""))
                return dateInFormat.parse(dateStr);
            else
                return null;
        } catch (ParseException e) {
            return null;
        }
    }


    @Override
    public EventType getEventType() {
        return EventType.MEDICATION;
    }

    public String getEventId() {
        return ID_KEY + medicationEvent.getAttribute(MEDICATION_ID);
    }

    public String getMedicationId() {
        return medicationEvent.getAttribute(MEDICATION_ID);
    }

    public int getDaysDuration() throws Exception {
        Date start = dateInFormat.parse(medicationEvent.getAttribute(START_DATE));
        String endDateStr = medicationEvent.getAttribute(END_DATE);
        Date end = dateInFormat.parse(endDateStr);
        return Days.daysBetween(new DateTime(start), new DateTime(end)).getDays();
    }

    public double getTotalDailyDosage() {
        try {
            return NumberUtils.createDouble(medicationEvent.getAttribute(TDD));
        } catch (Exception e) {
            return -1;
        }
    }

    public String getDosageFormCode() {
        return medicationEvent.getAttribute(DDI);
    }

    public Collection<String> getCategories() {
        return Indexer.getMedicationCategories(getMedicationId());
    }

    public Map<String, ArrayList<String>> getCategoryIDs() {
        return Indexer.getMedicationCategoryIDs(getMedicationId());
    }

    @Override
    public int getProviderId() {
        return NumberUtils.createInteger(medicationEvent.getAttribute(MOST_FREQ_PROVIDER));
    }


    @Override
    public double getValue() {
        return 0;  //todo: better way?
    }

    public String getCompressedLine() {
        return medicationEvent.getAttribute(COMPRESSED_LINE);
    }



    @Override
    public void updateDocument(SolrInputDocument doc) throws Exception {
        doc.addField("UniqueID", getUniqueId());
        doc.addField("EventID", getEventId());
        doc.addField("EventDate", getEventDateAsString());
        doc.addField("EventDateLong", getEventDateAsLong());
        if (getEndDate() != null) {      //end date can be null. Don't add these fields to index if so
            doc.addField("EndDateLong", getEndDateAsLong());
            doc.addField("DaysDuration", getDaysDuration());
        }
        double tdd = getTotalDailyDosage();
        if (tdd != -1)
            doc.addField("TotalDailyDosage", tdd);
        doc.addField("DosageFormCode", getDosageFormCode());
        Collection<String> categories = Indexer.getMedicationCategories(getMedicationId());
        doc.addField("Categories", categories);
        doc.addField("BucketID", getBuckets());

//        doc.addField("CompressedLine", getCompressedLine());

    }

    @Override
    public String toString() {
        return medicationEvent.toString();
    }
}
