package com.quntiles.vantage.denormalizer.contracts;

import com.quntiles.vantage.denormalizer.Indexer;
import com.quntiles.vantage.denormalizer.records.EventHelper;
import org.apache.solr.common.SolrInputDocument;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: mike
 * Date: 6/11/13
 * Time: 2:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class OutputPatient implements Comparable, Serializable {
    PatientInfo patientInfo;
    ProviderInfo providerInfo = null;
    Info infoObject = null;
    boolean primaryRow = false;
    Map<String, Set<Integer>> distanceVector = new HashMap<>();
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private double bmi;
    private double weight;
    private int patientAgeAtTimeOfEvent;
    private List<Integer> concurrentEvents = new ArrayList<>() ;

    public OutputPatient(){}

    public void setPatient(PatientInfo patient) {
        patientInfo = patient;
    }

    public void setProviderInfo(ProviderInfo provider) {
        providerInfo = provider;
    }

    public void setInfoObject(Info object) {
        infoObject = object;
    }


    public String getEventDateAsString() throws Exception {
        return infoObject.getEventDateAsString();
    }

    public Date getEventDate() throws Exception {
        return infoObject.getEventDate();
    }


    public long getEventDateAsLong() throws Exception {
        return infoObject.getEventDateAsLong();
    }

    public long getUniqueId() {
        return infoObject.getUniqueId();
    }

    public Info getInfoObject() {
        return infoObject;
    }
    @Override
    public int compareTo(Object o) {
        try {
            return getEventDateAsString().compareTo(((OutputPatient) o).getEventDateAsString());
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     *
     * @param infoObject            the eventId (actually the object) representing the distance vector
     * @param vectorTypePrefix      this is "" meaning the vector is for regular eventdate or "EndDate_" meaning vector is for end date
     * @param value                 the actual distance, +ve or -ve
     *                      eg MED:1032,"EndDate_",22 will set a distance vector of EndDate_MED_1032_i with value of 22
     */
    public void setDistanceVector(Info infoObject, String vectorTypePrefix, Integer value) {
        //derive the correct key
        String key=null;
        if (infoObject instanceof VitalsInfo) {
            key = infoObject.getEventId() + vectorTypePrefix +   "_" + infoObject.getValue();
        } else {
            key = infoObject.getEventId() + vectorTypePrefix ;
        }

        //same key (representing same EventID) can be repeated at different temporal distances .
        //So distance vector can have multiple distances for same key
        //retrieve any existing multi-value
        Set<Integer> distances = distanceVector.get(key);
        if (distances == null) {
            distances = new HashSet<Integer>();
        }
        distances.add(value);
        distanceVector.put(key, distances);

        //create additional distance vectors for MED Groups and ICD9 Codegroups
        //these vectors have the same value as the base vector but the key name is derived from the group ids
        Map<String, ArrayList<String>> groupVectors = null;

        if (infoObject instanceof MedicationInfo) {
            //obtain the categories for which distance vector needs to be set
            MedicationInfo medicationInfo = (MedicationInfo) infoObject;
            groupVectors = medicationInfo.getCategoryIDs();
        } else if (infoObject instanceof DiagnosisInfo) {
            //obtain and set the ICD9 codes for which distance vector needs to be set
            DiagnosisInfo diagnosisInfo = (DiagnosisInfo) infoObject;
            groupVectors = Indexer.getICD9CodeIDs(diagnosisInfo.getCodeId());
        }

        if (groupVectors != null) {
            for (String s : groupVectors.keySet()) {
                //each element of the map represents a group/subgroup etc
                //a distance vector is created for each
                ArrayList<String> idValues = groupVectors.get(s);
                for (String idValue : idValues) {
                    String groupKey = s + "_" + idValue;
                    Set<Integer> groupDistances = distanceVector.get(groupKey);
                    if (groupDistances == null) {
                        groupDistances = new HashSet<Integer>();
                    }
                    groupDistances.add(value);
                    distanceVector.put(groupKey, groupDistances);
                }
            }
        }


    }


    public SolrInputDocument getDoc() {
        try {
            SolrInputDocument document = new SolrInputDocument();

            patientInfo.updateDocument(document);
            if (providerInfo != null)
                providerInfo.updateDocument(document);

            if (infoObject == null) {
                document.addField("UniqueID", patientInfo.getUniqueId());
                document.addField("EventID", "PATIENT");
            } else {
                infoObject.updateDocument(document);
            }

            if (bmi != -1)
                document.addField("BMI", bmi);
            if (weight != -1)
                document.addField("Weight", weight);

            if (primaryRow) // Mark a single row per a patient as primary, so we can quickly filter for counts
                document.addField("PrimaryRow", "true");

            if (patientAgeAtTimeOfEvent != 0)
                document.addField("AgeAtEvent",patientAgeAtTimeOfEvent);

            if (!concurrentEvents.isEmpty()) {
                for (Integer eventId : concurrentEvents) {
                    document.addField("OccurredAtEventDate" , eventId);

                }
            }
            if (!distanceVector.isEmpty()) {
                for (String s : distanceVector.keySet()) {
                    Set<Integer> vectors = distanceVector.get(s);
//                    System.out.println(s + "=>" + vectors);
                    for (Integer distance : vectors) {
                        String newString = EventHelper.cleanseFieldName(s);
                        document.addField(newString+"_i",distance);
                    }
                }
            }

            return document;
        } catch (Exception e) {
            e.printStackTrace();
            return new SolrInputDocument();
        }
    }
    

    @Override
    public String toString() {
        if (infoObject == null)
            return patientInfo.toString();
        return patientInfo.toString() + "\n" + infoObject.toString();
    }

    public void setPrimaryRow() {
        primaryRow = true;
    }

    public void setBmi(double bmi) {
        this.bmi = bmi;
    }

    public double getBmi() {
        return bmi;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public int getPatientAgeAtTimeOfEvent() {
        return patientAgeAtTimeOfEvent;
    }

    public void setPatientAgeAtTimeOfEvent(int currentYear, int dOBYear) {
        if (dOBYear != 0 && currentYear != 0)
            this.patientAgeAtTimeOfEvent = currentYear - dOBYear ;
        else
            this.patientAgeAtTimeOfEvent = 0;
    }
}
