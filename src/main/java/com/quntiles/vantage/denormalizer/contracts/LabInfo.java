package com.quntiles.vantage.denormalizer.contracts;

import com.quntiles.vantage.denormalizer.records.EventType;
import org.apache.solr.common.SolrInputDocument;

import java.text.ParseException;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: mike
 * Date: 6/10/13
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class LabInfo extends Info {
    private static final String ID_KEY = "LAB:";
    private static final int PANEL_ID = 1;
    private static final int TEST_ID = 2;
    private static final int STATUS = 3;
    private static final int ABNORMAL_FLAG = 4;
    private static final int VALUE = 5;
    private static final int UNIT = 6;
    private static final int REFERENCE_RANGE = 7;
    private static final int PERFORMED_DATE = 8;
    private static final int REVIEWING_PROVIDER = 9;
    private static final int PRIMARY_CARE_PROVIDER = 10;
    private static final int MOST_FREQ_PROVIDER = 11;
    private static final int PANEL = 12;
    private static final int TEST = 13;
    private static final int COMPRESSED_LINE = 21;
    private boolean valid=true;

    public boolean isValid() {
        return valid;
    }

    private Event labEvent;

    public LabInfo(){}

    public LabInfo(Event labInfo) throws Exception {
        labEvent = labInfo;
    }

    public long getUniqueId() {
        return Long.parseLong(labEvent.getValue(0));
    }

    public String getEventDateAsString() {
        try {
            return dateOutFormat.format(dateInFormat.parse(labEvent.getAttribute(PERFORMED_DATE)));
        } catch (ParseException e) {
            valid=false;
        }
        return null;
    }


    @Override
    public Date getEventDate()  {
        try {
            return dateInFormat.parse(labEvent.getAttribute(PERFORMED_DATE));
        } catch (ParseException e) {
            valid=false;
        }
        return null;
    }

    @Override
    public Date getStartDate()  {
        return getEventDate();
    }

    @Override
    public Date getEndDate()  {
        return null;      //end dates are null for vitals, labs, diag
    }

//    public long getEventDateAsLong() throws Exception {
//        return dateOutFormat.parse(getEventDateAsString()).getTime();
//    }

    public String getEventId() {
        return ID_KEY + labEvent.getAttribute(TEST_ID);
    }

    @Override
    public EventType getEventType() {
        return EventType.LAB;
    }

    public int getPanelID() {
        //CV :don't propagate exception
        try {
            return Integer.parseInt(labEvent.getAttribute(PANEL_ID));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public String getStatus() {
        return labEvent.getAttribute(STATUS);
    }

    public String getAbnormalFlag() {
        return labEvent.getAttribute(ABNORMAL_FLAG);
    }

    @Override
    public double getValue() {
        try {
        return Double.parseDouble(labEvent.getAttribute(VALUE));
        } catch (Exception e) {
            return Double.MIN_VALUE;
        }
    }

    public String getUnit() {
        return labEvent.getAttribute(UNIT);
    }

    public String getPanel() {
        return labEvent.getAttribute(PANEL);
    }

    public String getTest() {
        return labEvent.getAttribute(TEST);
    }

    public String getReferenceRange() {
        return labEvent.getAttribute(REFERENCE_RANGE);
    }

    @Override
    public int getProviderId() {
        return Integer.parseInt(labEvent.getAttribute(MOST_FREQ_PROVIDER));
    }

    public String getCompressedLine() {
        return labEvent.getAttribute(COMPRESSED_LINE);
    }

    @Override
    public void updateDocument(SolrInputDocument doc) throws Exception {
        doc.addField("UniqueID", getUniqueId());
        doc.addField("EventID", getEventId());
        doc.addField("EventDate", getEventDateAsString());
        doc.addField("EventDateLong", getEventDateAsLong());
        doc.addField("PanelID", getPanelID());
        doc.addField("Status", getStatus());
        doc.addField("AbnormalFlag", getAbnormalFlag());
        double value = getValue();
        if (value != Double.MIN_VALUE)
            doc.addField("Value", getValue());
        doc.addField("Unit", getUnit());
//        doc.addField("ReferenceRange", getReferenceRange());

        doc.addField("Panel", getPanel());
        doc.addField("Test", getTest());
        doc.addField("BucketID", getBuckets());
//        doc.addField("CompressedLine", getCompressedLine());

    }

    @Override
    public String toString() {
        return labEvent.toString();
    }
}
