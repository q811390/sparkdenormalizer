package com.quntiles.vantage.denormalizer.contracts;
//
//import org.apache.solr.common.SolrInputDocument;
//import org.joda.time.DateTime;

import org.apache.solr.common.SolrInputDocument;
import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: mike
 * Date: 6/10/13
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class PatientInfo implements Serializable{
    private final int DOB_YEAR = 1;
    private final int GENDER = 2;
    private final int ETHNICITY = 3;
    private final int ZIP3 = 4;
    private final int PRIMARY_CAR_PROVIDER = 5;
    private final int MOST_FREQ_PROVIDER = 7;
    private final int STATE = 8;
    private final int CURRENT_YEAR = new DateTime().getYear();
    private Event patientEvent;
    private boolean valid;

    public PatientInfo(){}

    public PatientInfo(Event patientData) {
        patientEvent = patientData;
    }
    //    CURRENT_YEAR = Calendar.getInstance().get(Calendar.YEAR);
//    System.currentTimeMillis()/1000/3600/24/365.25+1970 - todo: check which one performs better

    public boolean isValid() {
        return valid;
    }

    public long getUniqueId() {
        return Long.parseLong(patientEvent.getValue(0));
    }

    public int getPatientId() {
        return patientEvent.getPatientId();
    }

    public long getProviderId() {
        try {
            return Long.parseLong(patientEvent.getAttribute(MOST_FREQ_PROVIDER));
        } catch (NumberFormatException e) {

            valid = false;
            return -1;
        }
    }

    public String getState() {
        //try {
            return patientEvent.getAttribute(STATE);
//        } catch (Exception e) {
//            // For some reason, the dump excludes nulls, so sometimes this value doesn't
//            // exist in array
//            return "";
//        }
    }
//
    public int getAge() {
        //try {
            return CURRENT_YEAR - Integer.parseInt(patientEvent.getAttribute(DOB_YEAR));
//        } catch (Exception e) {
//            // Missing age is not that uncommon, don't report
//            return 0;
//        }
    }

    public String getGender() {
        return patientEvent.getAttribute(GENDER);
    }

    public String getEthnicity() {
        return patientEvent.getAttribute(ETHNICITY);
    }

    public String getZip3() {
        return patientEvent.getAttribute(ZIP3);
    }

    public void updateDocument(SolrInputDocument doc) {
        doc.addField("PatientID", getPatientId());
        doc.addField("ProviderID", getProviderId());
        doc.addField("State", getState());
        int intData = getAge();
        if (intData != 0)
            doc.addField("Age", intData);
        doc.addField("Gender", getGender());
        doc.addField("Ethnicity", getEthnicity());
        if (!getZip3().equals(""))
            doc.addField("ZIP3", getZip3());
    }

    public int getDOBYear() {
        //try {
            return Integer.parseInt(patientEvent.getAttribute(DOB_YEAR));
//        } catch (Exception e) {
//            // Missing age is not that uncommon, don't report
//            return 0;
//        }
    }

    public int getCurrentYear() {
        return CURRENT_YEAR;
    }

    @Override
    public String toString() {
        return patientEvent.toString();
    }
}
