package com.quntiles.vantage.denormalizer.contracts;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: mike
 * Date: 6/10/13
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class DiagnosisLine {
    public static final String COLUMN_DELIMITER = "\\|"; //pipe character needs to be escaped
    public int patientId;
    public int providerId;
    public int dataSourceId;
    public Date eventDate;
    public String codeId;
    public String status;
    public String type;
    public Date startDate;
    public Date endDate;
    public String category;
//    public String diagnosis;

    public DiagnosisLine(){}

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public DiagnosisLine(String patientData) throws Exception {
        String[] parts = patientData.split(COLUMN_DELIMITER);
        patientId = Integer.parseInt(parts[0]);
        try {
            providerId = Integer.parseInt(parts[1]);
        } catch (Exception e) {
//            e.printStackTrace();
        }
        dataSourceId = Integer.parseInt(parts[2]);
        eventDate = dateFormat.parse(parts[3]);
        codeId = "DIAG:" + parts[4];
        status = parts[5];
        type = parts[6];
        startDate = dateFormat.parse(parts[7]);
        endDate = dateFormat.parse(parts[8]);
        category = parts[9];
//        diagnosis = parts[10];
    }
}
