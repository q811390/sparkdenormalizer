package com.quntiles.vantage.denormalizer.contracts;

import org.apache.commons.lang.StringUtils;
import org.apache.solr.common.SolrInputDocument;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: mike
 * Date: 6/10/13
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProviderInfo implements Serializable{
    public  final String COLUMN_DELIMITER = "\\|"; //pipe character needs to be escaped
    private  int PROVIDER_ID = 0;
    private  int ZIP = 1;
    private  int PATIENT_COUNT = 2;
    private  int DATASOURCE_ID = 3;
    private  int SPECIALTY = 4;
    private  int CREDENTIAL = 5;
    private  int FIRST_NAME = 6;
    private  int LAST_NAME = 7;
    private  int SPECIALTY_DISPLAY = 8;
    private  int INVESTIGATOR_ID = 9;
    private String[] csv;
    private long provider_id = -1;

    public ProviderInfo(){}

    public ProviderInfo(String line) {
        csv = line.split(COLUMN_DELIMITER);
    }

    public long getProviderId() {
        if (provider_id == -1) {
            try {
                provider_id = Long.parseLong(csv[PROVIDER_ID]);
            } catch (Exception e) {
                provider_id = 0;
            }
        }
        return provider_id;
    }

    public String getProviderZip() {
        return csv[ZIP];
    }

    public String getSpecialty() {
        try {
            return csv[SPECIALTY];
        } catch (Exception e) {
            return "";
        }
    }

    public String getCredentials() {
        try {
            return csv[CREDENTIAL];
        } catch (Exception e) {
            return "";
        }
    }

    public String getFirstName() {
        try {
            return csv[FIRST_NAME];
        } catch (Exception e) {
            return "";
        }
    }

    public String getLastName() {
        try {
            return csv[LAST_NAME];
        } catch (Exception e) {
            return "";
        }
    }

    public String getSpecialtyDisplay() {
        try {
            return csv[SPECIALTY_DISPLAY];
        } catch (Exception e) {
            return "";
        }
    }

    public String getInvestigatorId() {
        try {
            return csv[INVESTIGATOR_ID];
        } catch (Exception e) {
            return "";
        }
    }

    public void updateDocument(SolrInputDocument doc) {
//        if (!csv[ZIP].equals(""))
//            doc.addField("ProviderZip", csv[ZIP]);
//        if (!getSpecialty().equals(""))
//            doc.addField("Specialty", getSpecialty());
//        if (!getCredentials().equals(""))
//            doc.addField("Credential", getCredentials());
//        if (!getFirstName().equals(""))
//            doc.addField("ProviderFirstName", getFirstName());
//        if (!getLastName().equals(""))
//            doc.addField("ProviderLastName", getLastName());
        if (!getSpecialtyDisplay().equals(""))
            doc.addField("SpecialtyDisplay", getSpecialtyDisplay());
        if (!getInvestigatorId().equals(""))
            doc.addField("InvestigatorID", getInvestigatorId());
    }

    @Override
    public String toString() {
        return StringUtils.join(csv, COLUMN_DELIMITER);
    }
}
