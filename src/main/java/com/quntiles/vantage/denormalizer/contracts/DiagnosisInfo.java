package com.quntiles.vantage.denormalizer.contracts;

import com.quntiles.vantage.denormalizer.Indexer;
import com.quntiles.vantage.denormalizer.records.EventType;
import org.apache.solr.common.SolrInputDocument;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: mike
 * Date: 6/10/13
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class DiagnosisInfo extends Info implements Serializable{
    /*
    AttributeColumnID	AttributeName	ValueDescription	ValueDataType
1	CodeID	CodeID	int
2	Status	The status from the EHR provider.	text
3	Type	The type classification for the diagnosis.	text
4	OnsetDate	Date of the condition onset.	datetime
5	ResolvedDate	Date the condition was resolved.	datetime
6	Documenting Provider	Provider who documented the problem. This comes from the ASRaw.Problem table.	int
7	Primary Care Provider	The patient's primary care provider. This comes from the ASRaw.Patient table.	int
8	Most Frequent Provider	The most common provider calculated for this patient.	int
9	Name	Name of the diagnosis.	text
10	Category	Diagnosis category.	text
     */
    private  int EVENT_DATE = 0;
    private  String ID_KEY = "DIAG:";
    private  int CODE_ID = 1;
    private  int STATUS = 2;
    private  int TYPE = 3;
    private  int ONSET_DATE = 4;
    private  int RESOLVED_DATE = 5;
    private  int DOCUMENTING_PROVIDER = 6;
    private  int PRIMARY_CARE_PROVIDER = 7;
    private  int MOST_PREQ_PROVIDER = 8;
    private  int NAME = 9;
    private  int CATEGORY = 10;
    private  int COMPRESSED_LINE = 21;
    private boolean valid = true;
    private Event diagnosisEvent;

    public boolean isValid() {
        return valid;
    }

    public DiagnosisInfo(){}

    public DiagnosisInfo(Event diagnosisInfo) throws Exception {
        diagnosisEvent = diagnosisInfo;
    }

    public long getUniqueId() {
        return Long.parseLong(diagnosisEvent.getValue(0));
    }

    public String getEventDateAsString()  {
        try {
            return dateOutFormat.format(dateInFormat.parse(diagnosisEvent.getAttribute(EVENT_DATE)));
        } catch (ParseException e) {
           valid=false;
        }
        return null;
    }

    @Override
    public Date getEventDate() {
        try {
            return dateInFormat.parse( diagnosisEvent.getAttribute(EVENT_DATE) );
        } catch (ParseException e) {
            valid=false;
        }
        return null;
    }

//    public long getEventDateAsLong() throws Exception {
//        return dateOutFormat.parse(getEventDateAsString()).getTime();
//    }

    @Override
    public Date getStartDate() throws Exception {
        return getEventDate();
    }

    @Override
    public Date getEndDate() throws Exception {
        return null;      //end dates are null for vitals, labs, diag
    }

    public String getEventId() {
        return ID_KEY + diagnosisEvent.getAttribute(CODE_ID);
    }

    @Override
    public EventType getEventType() {
        return EventType.DIAGNOSIS;
    }

    public String getCodeId() {
        return diagnosisEvent.getAttribute(CODE_ID);
    }

    public String getName() {
        return diagnosisEvent.getAttribute(NAME);
    }

    public String getCategory() {
        return diagnosisEvent.getAttribute(CATEGORY);
    }

    public String getType() {
        return diagnosisEvent.getAttribute(TYPE);
    }

    public String getResolvedDateAsString() {
        return diagnosisEvent.getAttribute(RESOLVED_DATE);
    }

    public String getCompressedLine() {
        return diagnosisEvent.getAttribute(COMPRESSED_LINE);
    }

    @Override
    public int getProviderId() {
        return Integer.parseInt(diagnosisEvent.getAttribute(MOST_PREQ_PROVIDER));
    }

    @Override
    public double getValue() {
        return 0;  //Todo:see if there is a better way
    }

    @Override
    public void updateDocument(SolrInputDocument doc) throws Exception {
        doc.addField("UniqueID", getUniqueId());
        doc.addField("EventID", getEventId());
        doc.addField("EventDate", getEventDateAsString());
        doc.addField("EventDateLong", getEventDateAsLong());
        doc.addField("Name", getName());
        doc.addField("Category", getCategory());
        doc.addField("Type", getType());
        doc.addField("ResolvedDate", getResolvedDateAsString());

        doc.addField("ICD9Codes", Indexer.getICD9Codes(getCodeId()));
        doc.addField("BucketID", getBuckets());
//        doc.addField("ICD9CodeIDs", Program.ICD9.getICD9CodeIDs(getCodeId()));
//        doc.addField("CompressedLine", getCompressedLine());

    }

    @Override
    public String toString() {
        return diagnosisEvent.toString();
    }
}
