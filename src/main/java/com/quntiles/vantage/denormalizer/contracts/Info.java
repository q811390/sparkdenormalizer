package com.quntiles.vantage.denormalizer.contracts;

import com.quntiles.vantage.denormalizer.records.EventType;
import org.apache.solr.common.SolrInputDocument;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: mike
 * Date: 8/23/13
 * Time: 5:13 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Info implements Serializable {
    public static final String ISO_8601_DATEFORMAT = "yyyy-MM-dd'T'hh:mm:ssX";
    protected static final SimpleDateFormat isoDateFormat = new SimpleDateFormat(ISO_8601_DATEFORMAT);
    public static final int DAY_DIVISOR = 1000 * 60 * 60 * 24;    //to convert date as days from epoch
    protected static final SimpleDateFormat dateInFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    protected static final SimpleDateFormat dateOutFormat = new SimpleDateFormat("yyyy-MM-dd'T'00:00:00'Z'");

    Set<String> designatedBucket = null;

    public void setBuckets(Set<String> s) {
        designatedBucket = s;
    }

    public Info(){}

    public Set<String> getBuckets() {
        return designatedBucket;
    }
    public abstract void updateDocument(SolrInputDocument doc) throws Exception;
    public abstract long getUniqueId();
    public abstract String getEventDateAsString() throws Exception;
    public abstract String getEventId();
    public abstract Date getEventDate() throws Exception;
    public abstract EventType getEventType();
    public abstract Date getStartDate() throws Exception;
    public abstract Date getEndDate() throws Exception;

    public int getEventYear()  {
            int eventYear = 0;
            try {
                eventYear = new DateTime(getEventDate()).getYear();
            } catch (Exception e) {
            }
            return eventYear;
    }

    public long getEndDateAsLong() throws Exception {

        Date thisDate = getEndDate();
        if (thisDate != null) {
            long dateLong = (thisDate.getTime() / DAY_DIVISOR);
            return dateLong;
        } else {
            return -1;  //indicator for null date
        }

    }

    public long getEventDateAsLong() throws Exception{

        Date thisDate = getEventDate();
        long dateLong = (thisDate.getTime() / DAY_DIVISOR);
        return dateLong;
    }
    public abstract int getProviderId() ;
    public abstract double getValue();
}
