package com.quntiles.vantage.denormalizer.contracts;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mike
 * Date: 6/10/13
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class PatientLine {
    public static final String COLUMN_DELIMITER = "\\|"; //pipe character needs to be escaped
    public int patientId;
    public int providerId;
    public int dataSourceId;
    public String state;
    public String dobYear;
    public String gender;
    public String ethnicity;
    public int zip3;
    public int mostFreqProviderId;

    public PatientLine(){}

    public PatientLine(String patientData) throws Exception {
        String[] parts = patientData.split(COLUMN_DELIMITER);
        patientId = Integer.parseInt(parts[0]);
        try {
            providerId = Integer.parseInt(parts[1]);
        } catch (Exception e) {
//            e.printStackTrace();
        }
        dataSourceId = Integer.parseInt(parts[2]);
        state = parts[3];
        dobYear = parts[4];
        gender = parts[5];
        ethnicity = parts[6];
        zip3 = Integer.parseInt(parts[7]);
        if (parts.length > 8)
            mostFreqProviderId = Integer.parseInt(parts[8]);
    }

    @Override
    public String toString() {
        List<String> parts = new ArrayList<String>();
        parts.add(String.valueOf(patientId));
        parts.add(String.valueOf(providerId));
        parts.add(String.valueOf(dataSourceId));
        parts.add(state);
        parts.add(dobYear);
        parts.add(gender);
        parts.add(ethnicity);
        parts.add(String.valueOf(zip3));
        parts.add(String.valueOf(mostFreqProviderId));
        return StringUtils.join(parts, COLUMN_DELIMITER);
    }
}
