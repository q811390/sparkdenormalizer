package com.quntiles.vantage.denormalizer;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: mike
 * Date: 8/24/13
 * Time: 10:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class ICD9 implements Serializable{
    public  final String COLUMN_DELIMITER = "\\|"; //pipe character needs to be escaped
    private  int ICD9STRING_INDEX = 2;
    private  int ICD9CODE_INDEX = 7;

    public ICD9(){}

    //    private HashMap<String, ICD9Code> ICD9Codes = new HashMap<String, ICD9Code>();
    private HashMap<String, Map<String,ArrayList<String>>> ICD9CodeIDs =     new HashMap<>();
    private HashMap<String, List<String>> ICD9CodeStrings = new HashMap<String, List<String>>();
    private  String[] idNames =  { "DIAG_Group", "DIAG_Class", "DIAG_SubClass", "DIAG_SubClass1" , "DIAG_SubClass2" };

    public ICD9(String filename,List<String> data) throws Exception {
        System.out.println();
        System.out.println("Loading ICD9 codes.");

        if(data == null) {
            Path filePath = Paths.get(filename);
            if (!Files.exists(filePath)) {
                System.out.println("Failed to open ICD9 codes file: " + filename);
                System.out.println("Program exiting");
                System.exit(0);
            }

            BufferedReader reader = Files.newBufferedReader(filePath, Charset.defaultCharset());

            String line;
//        String[] lr;
            List<String> codestrings, codestringparts;
            Map<String, ArrayList<String>> codeids;
            while ((line = reader.readLine()) != null) {
                processLine( line);
            }
        }else{
            for (String line : data) {
                processLine(line);
            }
        }

        System.out.println("Finished.");
    }

    private void processLine( String line) {
        List<String> codestrings;
        List<String> codestringparts;
        Map<String, ArrayList<String>> codeids;
        String[] parts = line.split(COLUMN_DELIMITER);
        if (parts.length < ICD9CODE_INDEX) {
           // System.err.println("ICD9 codes are missing from " + filename);
            // System.exit(0);
            return;
        }

        codestrings = new ArrayList<String>();
        codestringparts = new ArrayList<String>();

        //parse the IDC9 strings
        for (int x = ICD9STRING_INDEX; x < ICD9CODE_INDEX; x++) {
            codestringparts.add(parts[x]);
            codestrings.add(StringUtils.join(codestringparts, "/"));
        }

        //parse the IDC9 codes
        codeids = new HashMap<>();
        for (int x = ICD9CODE_INDEX, y = 0; x < parts.length; x++, y++) {
            ArrayList<String> aList = new ArrayList<String>();
            aList.add(parts[x]);
            codeids.put(idNames[y], aList); //store the components in an AL , there could be multiple per component
            // but there aren't right now
        }

        ICD9CodeStrings.put(parts[0], codestrings);
        ICD9CodeIDs.put(parts[0], codeids);
    }

    public Collection<String> getICD9Codes(String icd9CodeIDs) {
        return ICD9CodeStrings.get(icd9CodeIDs);
    }

//    public Collection<String> getICD9Codes(String icd9CodeIDs) {
//        return ICD9CodeIDs.get(icd9CodeIDs).getICD9Codes();
//    }
//
    public Map<String,ArrayList<String>> getICD9CodeIDs(String icd9CodeIDs) {
        return ICD9CodeIDs.get(icd9CodeIDs);
    }
    
//
//    private class ICD9Code {
//        String ICD9Code;
//        String ICD9CodeID;
//
//        ICD9Code Parent;
//
//        ArrayList<ICD9Code> Children = new ArrayList<ICD9.ICD9Code>();
//
//        public ICD9Code(String icd9Code, String icd9CodeId) {
//            this(icd9Code, icd9CodeId, null);
//        }
//
//        public ICD9Code(String icd9Code, String icd9CodeId, ICD9Code parent) {
//            ICD9Code = icd9Code;
//            ICD9CodeID = icd9CodeId;
//            Parent = parent;
//        }
//
//        public void addParent(ICD9Code icd9Code) {
//            Parent = icd9Code;
//        }
//
//        public Collection<String> getICD9Codes() {
//            ArrayList<String> codes = new ArrayList<String>();
//
//            codes.add(ICD9Code);
//            if (Parent != null)
//                codes.addAll(Parent.getICD9Codes());
//
//            return codes;
//        }
//
//        public Collection<String> getICD9CodeIDs() {
//            ArrayList<String> codes = new ArrayList<String>();
//
//            codes.add(ICD9Code);
//            if (Parent != null)
//                codes.addAll(Parent.getICD9CodeIDs());
//
//            return codes;
//        }
//    }
}
