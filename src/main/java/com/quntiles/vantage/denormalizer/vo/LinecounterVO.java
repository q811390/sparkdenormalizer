package com.quntiles.vantage.denormalizer.vo;

import com.quntiles.vantage.denormalizer.records.PatientRecord;

import java.util.List;

/**
 * Created by q811390 on 4/15/2015.
 */
public  class LinecounterVO{

    long lineCounter;

    public long getLineCounter() {
        return lineCounter;
    }

    public List<PatientRecord> getPatientRecords() {
        return patientRecords;
    }

    List<PatientRecord> patientRecords;
    public LinecounterVO(long lineCounter, List<PatientRecord> patientRecords){
        this.lineCounter=lineCounter;
        this.patientRecords=patientRecords;
    }
}
