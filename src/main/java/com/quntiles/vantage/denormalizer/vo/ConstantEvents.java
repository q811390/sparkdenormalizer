package com.quntiles.vantage.denormalizer.vo;

import com.quntiles.vantage.denormalizer.DosageFormsFilterList;
import com.quntiles.vantage.denormalizer.ICD9;
import com.quntiles.vantage.denormalizer.Medications;
import com.quntiles.vantage.denormalizer.ProviderData;
import com.quntiles.vantage.denormalizer.contracts.Event;

/**
 * Created by q811390 on 4/21/2015.
 */
public class ConstantEvents {

    private Medications medications = null;
    private ProviderData providers = null;
    private DosageFormsFilterList dosageFormsFilterList = null;
    private ICD9 icd9 = null;
    private Event event = null;

    public ConstantEvents(Medications medications, ProviderData providers, DosageFormsFilterList dosageFormsFilterList, ICD9 icd9) {
        this.medications = medications;
        this.providers = providers;
        this.dosageFormsFilterList = dosageFormsFilterList;
        this.icd9 = icd9;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Medications getMedications() {
        return medications;
    }

    public void setMedications(Medications medications) {
        this.medications = medications;
    }

    public ProviderData getProviders() {
        return providers;
    }

    public void setProviders(ProviderData providers) {
        this.providers = providers;
    }

    public DosageFormsFilterList getDosageFormsFilterList() {
        return dosageFormsFilterList;
    }

    public void setDosageFormsFilterList(DosageFormsFilterList dosageFormsFilterList) {
        this.dosageFormsFilterList = dosageFormsFilterList;
    }

    public ICD9 getIcd9() {
        return icd9;
    }

    public void setIcd9(ICD9 icd9) {
        this.icd9 = icd9;
    }
}
