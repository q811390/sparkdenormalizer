package com.quntiles.vantage.denormalizer;

import java.io.BufferedReader;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: mike
 * Date: 8/24/13
 * Time: 10:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class Medications implements Serializable{

    public  final String COLUMN_DELIMITER = "\\|"; //pipe character needs to be escaped
    private HashMap<String, Set<String>> map = new HashMap<String, Set<String>>();
    private  final String[] idNames = {"MED_GroupID", "MED_ClassId", "MED_SubClassId", };

    public Medications(){}

    public Medications(String filename, List<String> data) throws Exception {
        System.out.println();
        System.out.println("Loading medications.");

        if(data == null) {
            Path filePath = Paths.get(filename);
            if (!Files.exists(filePath)) {
                System.out.println("Failed to open medications file: " + filename);
                System.out.println("Program exiting");
                System.exit(0);
            }
            BufferedReader reader = Files.newBufferedReader(filePath, Charset.defaultCharset());
            String line;
            while ((line = reader.readLine()) != null) {
                processLine(line);
            }
        }else {
            for (String line : data) {
                processLine(line);
            }
        }
        System.out.println("Finished.");
    }

    private void processLine(String line) {
        String[] parts = line.split(COLUMN_DELIMITER);

        if (!map.containsKey(parts[4])) {
            map.put(parts[4], new HashSet<String>());
        }

        Set<String> set = map.get(parts[4]);
        set.add(parts[5]);
        set.add(parts[5] + "/" + parts[6]);
        set.add(parts[5] + "/" + parts[6] + "/" + parts[7]);
    }

    public Collection<String> getCategories(String medicationId) {
        return map.get(medicationId);
    }

    public Map<String,ArrayList<String>> getCategoryIDs(String medicationId) {
            
        Collection<String > unSortedFullSet = map.get(medicationId);
        Set<String> unSortedSet = new HashSet<>() ;
        if (unSortedFullSet != null)     {
            for (Iterator<String> iterator = unSortedFullSet.iterator(); iterator.hasNext(); ) {
                String next = iterator.next();
                if (unSortedSet.isEmpty())
                    unSortedSet.add(next);
                else {
                    boolean isFound = false;
                    for (String s : unSortedSet) {
                        if (next.contains(s)) {
                            unSortedSet.add(next);
                            unSortedSet.remove(s);
                            isFound = true;
                            break;
                        } else if (s.contains(next))
                            isFound = true;

                    }
                    if (!isFound)
                        unSortedSet.add(next);
                }
            }
        }
        if (unSortedSet != null) {
            Map<String, ArrayList<String>> categoryIDs = new HashMap<>();
            for (String unParsed : unSortedSet) {
                    String[] parsed = unParsed.split("/");
                //todo: ensure there are just the right number of elements
                for (int i = 0; i < parsed.length; i++) {
                    String s = parsed[i];
                    ArrayList previous = categoryIDs.get(idNames[i]);
                    if ( previous != null )      {
                        previous.add(s);
                        categoryIDs.put(idNames[i], previous );   //there could be multiple
                    } else {
                        ArrayList<String> newList = new ArrayList();
                        newList.add(s);
                        categoryIDs.put(idNames[i],newList);
                    }
                }

            }

                return categoryIDs;
        }

        return  null;
    }

}
