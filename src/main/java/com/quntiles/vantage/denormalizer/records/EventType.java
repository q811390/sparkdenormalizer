package com.quntiles.vantage.denormalizer.records;

/**
 * Created with IntelliJ IDEA.
 * User: cvora
 * Date: 2/28/14
 * Time: 9:39 AM
 * To change this template use File | Settings | File Templates.
 */
public enum EventType {

    MEDICATION(1),
    DIAGNOSIS(2),
    LAB(4),
    VITALS(6);

    private int eventTypeID;

    EventType(int typeID){
        eventTypeID = typeID;
    }

    public int getEventType() {
        return eventTypeID;
    }

    }
