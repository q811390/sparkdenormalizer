package com.quntiles.vantage.denormalizer.records;

/**
 * Created with IntelliJ IDEA.
 * User: cvora
 * Date: 2/26/14
 * Time: 3:13 PM
 * To change this template use File | Settings | File Templates.
 */
public enum PatientRecordFields {
    UNIQUEID("UniqueID"),
    EVENTDATE("EventDate"),
    EVENTID("EventID"),
    PRIMARYROW("PrimaryRow"),
    PATIENTID("PatientID"),
    PROVIDERID("ProviderID"),
    STATE("State"),
    AGE("Age"),
    AGEATEVENT("AgeAtEvent"),
    OCCURREDATEVENTDATE("OccurredAtEventDate"),
    GENDER("Gender"),
    ETHNICITY("Ethnicity"),
    ZIP3("Zip3"),
    BMI("BMI"),
    WEIGHT("Weight"),
    SPECIALTYDISPLAY("SpecialtyDisplay"),
    INVESTIGATORID("InvestigatorID"),
    TYPE("Type"),
    RESOLVEDDATE("ResolvedDate"),
    NAME("Name"),
    CATEGORY("Category"),
    ICD9CODES("ICD9Codes"),
    ICD9CODEIDS("IDC9CodeIDs"),
    DAYSDURATION("DaysDuration"),
    TOTALDAILYDOSAGE("TotalDailyDosage"),
    DOSAGEFORMCODE("DosageFormCode"),
    CATEGORIES("Categories"),
    PERFORMEDDATE("PerformedDate"),
    PANELID("PanelID"),
    ABNORMALFLAG("AbnormalFlag"),
    PANEL("Panel"),
    TEST("Test"),
    VALUE("Value"),
    UNIT("Unit"),
    STATUS("Status")  ;
    private final String fieldName;
    PatientRecordFields(String name) {
        fieldName = name;
    }
    public  String getFieldName() { return fieldName; }

}
