package com.quntiles.vantage.denormalizer.records;

import com.quntiles.vantage.denormalizer.Indexer;
import com.quntiles.vantage.denormalizer.contracts.*;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;
import org.joda.time.Period;

import java.io.Serializable;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: mike
 * Date: 6/10/13
 * Time: 2:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class PatientRecord implements Serializable{
    private PatientInfo patient;
    public ProviderInfo providerInfo;
    public List<DiagnosisInfo> diagnosisInfos = new ArrayList();
    public List<MedicationInfo> medicationInfos = new ArrayList();
    public List<VitalsInfo> vitalsInfos = new ArrayList();
    public List<LabInfo> labInfos = new ArrayList();
    public Set<String> denormalizedEventsList = new HashSet<String>();

    private Date lastWeightDate = new Date(Long.MIN_VALUE);
    private double lastWeight = -1;
    private Date lastBmiDate = new Date(Long.MIN_VALUE);
    private double lastBmi = -1;
    private boolean calculateVectors = true;

    public PatientRecord(){}

    //build a patient record using a query response
    public PatientRecord(QueryResponse response) {
        System.out.println("in ctr");
        SolrDocumentList docs = response.getResults();
        for (SolrDocument doc : docs) {
            try {
                addSolrEvent(doc);
            } catch (Exception e) {
                e.printStackTrace();  //log this instead if necessary
            }
//            Event e = new Event(doc);
        }

    }

    public PatientRecord(boolean v) {
        calculateVectors = v;
    }

    public void setPatient(PatientInfo info) {
        patient = info;
    }

    public PatientInfo getPatient() {
        return patient;
    }

    public boolean isExistingPatient() {
        //
        return false;
    }
    public int getCurrentYear() {
        if (patient != null)
            return patient.getCurrentYear();
        return 0;

    }
    public void addDiagnosisEvent(DiagnosisInfo diagnosisInfo) {
        diagnosisInfos.add(diagnosisInfo);
        denormalizedEventsList.add(diagnosisInfo.getEventId());
    }

    public void addMedicationEvent(MedicationInfo medicationInfo) {
        medicationInfos.add(medicationInfo);
        denormalizedEventsList.add(medicationInfo.getEventId());
    }

    private void addLabEvent(LabInfo labInfo) {
        labInfos.add(labInfo);
        denormalizedEventsList.add(labInfo.getEventId());
    }

    public void addVitalEvent(VitalsInfo vitalsInfo) {
        vitalsInfos.add(vitalsInfo);
        String eventId = vitalsInfo.getEventId();
        if (eventId.equals("VITAL:Weight") && lastWeightDate.compareTo(vitalsInfo.getEventDate()) < 0) {
            lastWeightDate = vitalsInfo.getEventDate();
            lastWeight = vitalsInfo.getValue();
        }
        if (eventId.equals("VITAL:BMI") && lastBmiDate.compareTo(vitalsInfo.getEventDate()) < 0) {
            lastBmiDate = vitalsInfo.getEventDate();
            lastBmi = vitalsInfo.getValue();
        }
        denormalizedEventsList.add(eventId);
    }



    public List<OutputPatient> getSchemaLines() throws Exception {
//        DateTime start = new DateTime();

        List<OutputPatient> lines = new ArrayList<OutputPatient>();
        if (patient == null)
            return lines;

        for (DiagnosisInfo diag : diagnosisInfos) {
            OutputPatient line = new OutputPatient();
            line.setPatient(patient);
            line.setProviderInfo(Indexer.getPROVIDERS().getProviderInfo(patient.getProviderId()));
            line.setBmi(lastBmi);
            line.setWeight(lastWeight);
            line.setInfoObject(diag);
            line.setPatientAgeAtTimeOfEvent(diag.getEventYear(), patient.getDOBYear());
            lines.add(line);
        }

        for (MedicationInfo med : medicationInfos) {
            OutputPatient line = new OutputPatient();
            line.setPatient(patient);
            line.setProviderInfo(Indexer.getPROVIDERS().getProviderInfo(patient.getProviderId()));
            line.setBmi(lastBmi);
            line.setWeight(lastWeight);
            line.setInfoObject(med);
            line.setPatientAgeAtTimeOfEvent(med.getEventYear(), patient.getDOBYear());
            lines.add(line);
        }

        for (VitalsInfo vital : vitalsInfos) {
            OutputPatient line = new OutputPatient();
            line.setPatient(patient);
            line.setProviderInfo(Indexer.getPROVIDERS().getProviderInfo(patient.getProviderId()));
            line.setBmi(lastBmi);
            line.setWeight(lastWeight);
            line.setInfoObject(vital);
            line.setPatientAgeAtTimeOfEvent(vital.getEventYear(), patient.getDOBYear());
            lines.add(line);
        }

        for (LabInfo lab : labInfos) {
            OutputPatient line = new OutputPatient();
            line.setPatient(patient);
            line.setProviderInfo(Indexer.getPROVIDERS().getProviderInfo(patient.getProviderId()));
            line.setBmi(lastBmi);
            line.setWeight(lastWeight);
            line.setInfoObject(lab);
            line.setPatientAgeAtTimeOfEvent(lab.getEventYear(), patient.getDOBYear());
            lines.add(line);
        }

        Collections.sort(lines);

        if (lines.size() == 0) {
            OutputPatient line = new OutputPatient();
            line.setPatient(patient);
            line.setProviderInfo(Indexer.getPROVIDERS().getProviderInfo(patient.getProviderId()));
            line.setBmi(lastBmi);
            line.setWeight(lastWeight);
            lines.add(line);
        }

        lines.get(0).setPrimaryRow();
//        DateTime end = new DateTime();
//        Period p = new Period(start, end);
//        System.out.println("Patient " + patient.getPatientId() + " GetSchemaLines Elapsed=" + PeriodFormat.getDefault().print(p));

        //calculate distance vectors
        if (calculateVectors)
            computeDistanceVectors(lines);

        return lines;
    }

    //this method creates dynamic fields within each event to reflect the temporal distance to other fields
    //the algorithm basically finds all the possible event pairs in the list  -(n*n-1)/2 pairs possible
    //distance vectors for a pair are calculated only once
    private void computeDistanceVectors(List<OutputPatient> eventList) throws Exception {
        for (int i = 0; i < eventList.size(); i++) {
            //structure inner loop so that always distance to self is skipped and that pair that was already computed is not computed again
            // i.e since 2-3 is computed, 3-2 is skipped
            for (int j = i+1; j < eventList.size(); j++) {
                OutputPatient a = eventList.get(i);
                OutputPatient b =  eventList.get(j);
                setVectorPair(a,b);
            }
        }

//        long end = System.nanoTime();
//        DateTime end = new DateTime();
//        Period p = new Period(start,end);
//        System.out.println("Distance vector Elapsed=" + PeriodFormat.getDefault().print(p));
    }

    public void setVectorPair(OutputPatient thisPatientEvent, OutputPatient otherPatientEvent) throws Exception {
        //extract info objects
        Info thisInfo = thisPatientEvent.getInfoObject();
        Info otherInfo = otherPatientEvent.getInfoObject();
        //get dates as long
        long thisDateLong = thisPatientEvent.getEventDateAsLong();
        long otherDateLong = otherPatientEvent.getEventDateAsLong();
        //calculate distance in days
        int diff = (int)(otherDateLong - thisDateLong);


        //set distances on each event of the pair
        thisPatientEvent.setDistanceVector(otherInfo, "" , diff);
        otherPatientEvent.setDistanceVector(thisInfo, "" , -diff);//the distance for the other is negative



        //set distances on each event of the pair
//        if (thisPatientEvent.getInfoObject().getEventType().equals(EventType.MEDICATION))
//        {
//            long thisEndDateLong = thisPatientEvent.getEndDateAsLong();
//            //calculate distance in days
//            diff = (int) (otherDateLong - thisEndDateLong);
//            thisPatientEvent.setDistanceVector(otherInfo, "_EndDate", diff);
//            otherPatientEvent.setDistanceVector(thisInfo, "_EndDate", -diff);//the distance for the other is negative
//        }

        //determine concurrency of the event . First do this for the first event of the pair
        //i.e calculate if the other event was concurrent with this date
//        setConcurrentEvent(thisPatientEvent, otherPatientEvent, thisInfo, otherInfo);     //taken out for now

        return ;

    }


    //check if a date falls between a given start and end dates
    private Boolean checkDateOverlap(EventType thisType, Date firstStartDate, Date firstEndDate, EventType otherType, Date nextStartDate, Date nextEndDate) {
        if (firstStartDate == null ||  nextStartDate == null )
            return false;
        if (firstEndDate == null)
            firstEndDate = firstStartDate;
        if (nextEndDate == null)
            nextEndDate = nextStartDate;

        Interval interval1 = new Interval(new DateTime(firstStartDate),
                new DateTime(firstEndDate));
        Interval interval2 = new Interval(new DateTime(nextStartDate),
                new DateTime(nextEndDate));
        if (thisType.equals(otherType) && otherType.equals(EventType.MEDICATION)) {
            return interval1.overlaps(interval2);
        }
        else {
            return interval1.contains(new DateTime(nextStartDate)) || interval2.contains(new DateTime(firstStartDate));
        }
    }    




    @Override
    public String toString() {
        return "";
    }

    public void addEvent(Event event) throws Exception {
        // Switch on EventTypeID
        if (event.getEventId() != null)    {
            switch (event.getEventId().charAt(0)) {
                case '1':
                    MedicationInfo info = new MedicationInfo(event);
                    //info.setBuckets(bucket);
                    if(info.isValid())
                        addMedicationEvent(info);
                    break;
                case '2':
                    DiagnosisInfo diagnosisInfo = new DiagnosisInfo(event);
                   // diagnosisInfo.setBuckets(bucket);
                    if(diagnosisInfo.isValid())
                        addDiagnosisEvent(diagnosisInfo);
                    break;
                case '3':
                    patient = new PatientInfo(event);
                    break;
                case '4':
                    LabInfo labInfo = new LabInfo(event);
                    if(labInfo.isValid())
                        addLabEvent(labInfo);
                    break;
                case '5':
//                addProcedureEvent(event);
                    break;
                case '6':
                    VitalsInfo vitalsInfo = new VitalsInfo(event);
                    //vitalsInfo.setBuckets(bucket);
                    addVitalEvent(vitalsInfo);
                    break;
                default:
                    System.out.println();
                    System.out.println("=== ERROR ===");
                    System.out.println(event);
            }
        } else {
            System.err.println("getEventID was null");
        }
    }

    public void addSolrEvent(SolrDocument event) throws Exception {
        // Switch on EventTypeID
        if (event != null) {
            String type = (String)event.getFieldValue(PatientRecordFields.EVENTID.getFieldName());
            String[] eventTypes = type.split(":");
            switch (eventTypes[0])  {
                case "MED":
//                    MedicationInfo info = new MedicationInfo(event);
//                    addMedicationEvent(info);
                    break;
                case "DIAG":
//                    addDiagnosisEvent(new DiagnosisInfo(event));
                    break;
                case "LAB":
//                    addLabEvent(new LabInfo(event));
                    break;
                case "VITAL":
//                    addVitalEvent(new VitalsInfo(event));
                    break;
                default:
                    System.out.println();
                    System.out.println("=== ERROR ===");
                    System.out.println(event);
            }
        } else {
            System.err.println("getEventID was null");
        }
    }

    public static int getYearDifference(int currentYear, Date d2) {
        int years = new Period(new DateTime(currentYear), new DateTime(d2)).getYears();
        return years;
    }

    public static int findDaysDistance(Date d1, Date d2) {
        DateTime start= new DateTime(d1);
        DateTime end = new DateTime(d2);
        int days = Days.daysBetween(start.toLocalDate(), end.toLocalDate()).getDays();
        return days;
    }
}
