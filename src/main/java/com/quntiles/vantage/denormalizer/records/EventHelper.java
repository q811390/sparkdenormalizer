package com.quntiles.vantage.denormalizer.records;

import com.quntiles.vantage.denormalizer.contracts.*;
import org.apache.commons.lang3.StringUtils;

/**
 * Created with IntelliJ IDEA.
 * User: cvora
 * Date: 2/24/14
 * Time: 5:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class EventHelper {

    public static long getProviderId(Event event) throws Exception {
        Info info = null;
            // Switch on EventTypeID
            if (event.getEventId() != null) {
                switch (event.getEventId().charAt(0)) {
                    case '1':
                        info = new MedicationInfo(event);
                        return info.getProviderId();
                    case '2':
                        info = new DiagnosisInfo(event);
                        return info.getProviderId();
                    case '4':
                        info = new LabInfo(event);
                        return info.getProviderId();
                    case '6':
                        info = new VitalsInfo(event);
                        return info.getProviderId();
                    case '3':
                        PatientInfo patientInfo = new PatientInfo(event);
                        return patientInfo.getProviderId();
                    default:
                        System.out.println();
                        System.out.println("=== ERROR ===");
                        System.out.println(event);
                }
            } else {
                System.err.println("getEventID was null");
            }
        return -1;

    }

    public static String cleanseFieldName(String inputString) {
        if (inputString.contains("VITAL:"))
            inputString = StringUtils.replace(inputString, "VITAL:", "");
        if (inputString.contains("Calculated"))
            inputString = StringUtils.replace(inputString, "Calculated", "");
        if (inputString.contains(":"))
            inputString = StringUtils.replace(inputString, ":", "_");
        if (inputString.contains("."))
            inputString = StringUtils.replace(inputString, ".", "_");

        return inputString.trim();
    }
}
