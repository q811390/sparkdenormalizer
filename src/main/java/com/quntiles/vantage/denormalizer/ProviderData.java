package com.quntiles.vantage.denormalizer;

import com.quntiles.vantage.denormalizer.contracts.ProviderInfo;

import java.io.BufferedReader;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: mike
 * Date: 8/24/13
 * Time: 10:34 AM
 * CV: TODO: this has same function as the ProviderData class in NestedQueries project. Refactor to keep one class only
 */
public class ProviderData implements Serializable {

    private HashMap<Long, ProviderInfo> map = new HashMap<>();

    public ProviderData(){}

    public ProviderData(String filename, List<String> data) throws Exception {
        System.out.println();
        System.out.println("Loading providers using Denormalizer library.");

        if (data == null) {


            Path filePath = Paths.get(filename);
            if (!Files.exists(filePath)) {
                System.out.println("Failed to open providers file: " + filename);
                System.out.println("Program exiting");
                System.exit(0);
            }

            BufferedReader reader = Files.newBufferedReader(filePath, Charset.defaultCharset());

            String line;
            while ((line = reader.readLine()) != null) {
                processLine(line);
            }
        }
        else{
            for(String line : data ){
                processLine(line);
            }
        }
        System.out.println("Finished loading " + map.size() + " providers.");
    }

    private void processLine(String line) {
        ProviderInfo providerInfo = new ProviderInfo(line);
        map.put(providerInfo.getProviderId(), providerInfo);
    }

    public ProviderInfo getProviderInfo(long providerId) {
        return map.get(providerId);
    }
}
