package com.quintiles.denormalizer

/**
 * Created by q811390 on 5/20/2015.
 */


import scala.collection.mutable.ArrayBuffer
import scala.util.control.Breaks._


/**
 * Created by dhiraj on 3/25/15.
 */
object EqualPartitioner {


  def main(args: Array[String]) {

    //    val conf = new SparkConf()
    //      .setMaster("local[2]")
    //      .setAppName("PetFoodAnalysis")
    //    //.set("spark.executor.memory", "4g")
    //
    val keyMap: Map[Int, Int] = Map(1 -> 0, 2 -> 1, 3 -> 1, 7 -> 2, 8 -> 3)
    val valueMap: Map[Int, Int] = Map(1 -> 8, 2 -> 4, 3 -> 5, 7 -> 10, 8 -> 6)

    //
    //    val sc = new SparkContext(conf)
    //    val puppyRdd = sc.parallelize(List(1, 1, 1, 2, 3, 3, 3, 3, 3, 3, 4, 4, 7)).map(a => (a, a)).sortBy(a => a._1)
    //    val k = sc.broadcast(keyMap)
    //
    //    val tem = puppyRdd.partitionBy(new RangePartitioner(2, puppyRdd))
    //    print(tem.glom.collect().foreach(a => println(a.toList)))
    //
    //    println("-----------------")
    //
    //    val temm = puppyRdd.partitionBy(new ExactPartitioner(2, k.value))
    //    print(temm.glom.collect().foreach(a => println(a.toList)))
    variance(keyMap, valueMap, 6)
    // testEqualizeCounts()

  }

  def testEqualizeCounts(): Unit = {
    //
    val vv = Map(1 -> 4, 2 -> 4, 3 -> 4, 4 -> 2, 5 -> 2)
    val res = equalizeCounts(vv, 4)
    println(res.toList.sortBy(a => a._2))
    assert(res.toList.sortBy(a => a._2) == List((3, 0), (2, 1), (1, 2), (5, 3), (4, 3)))

    val vvv = Map(1 -> 1, 2 -> 4, 3 -> 4, 4 -> 2, 5 -> 2)
    val res1 = equalizeCounts(vvv, 4)
    println(res1.toList.sortBy(a => a._2))
    assert(res1.toList.sortBy(a => a._2) == List((3, 0), (2, 1), (1, 2), (4, 2), (5, 3)))

    val vv1 = Map(1 -> 10, 2 -> 13, 3 -> 14, 4 -> 4, 5 -> 20)
    val res2 = equalizeCounts(vv1, 4)
    println(res2.toList.sortBy(a => a._2))
    assert(res2.toList.sortBy(a => a._2) == List((5, 0), (3, 1), (2, 2), (4, 2), (1, 3)))


    val vv2 = Map(1 -> 10, 2 -> 13, 3 -> 14, 4 -> 4, 5 -> 20)
    val res3 = equalizeCounts(vv2, 3)
    println(res3.toList.sortBy(a => a._2))
    assert(res3.toList.sortBy(a => a._2) == List((5, 0), (3, 1), (4, 1), (1, 2), (2, 2)))

  }


//  def secondPass(memberBucketMap: Map[Int, Int], memberPatientCountMap: Map[Int, Int], avg: Int) :  Map[Int, Int] = {
//
//    val m = memberBucketMap.map(a => PatientBucketMap(a._1, a._2, memberPatientCountMap.get(a._1).get)).toList
//    val sorted = m.sortBy(a=> a.memberPatientCount).reverse
//
//    val groupedBy = m.groupBy(a=> a.bucketId).map{  case (k,v) => (k, v.sortBy(a=>a.memberPatientCount)) }  //(a => (a._1, a._2.sortBy(a._2)))
//
//    sorted.foreach(a => {
//        if(a.memberPatientCount > avg){
//
//
//        }
//
//    })
//
//
//    null
//  }


  case class PatientBucketMap(memberID: Int, bucketId: Int, memberPatientCount: Int)

  def variance(memberBucketMap: Map[Int, Int], memberPatientCountMap: Map[Int, Int], avg: Int): Double = {

    val m = memberBucketMap.map(a => PatientBucketMap(a._1, a._2, memberPatientCountMap.get(a._1).get)).toList
    //val sortedm = m.sortBy(a => (a.bucketId))
    val mm = m.groupBy(a => a.bucketId).map { case (k, v) => k -> (v map (c => c.memberPatientCount) sum) }.map(a => (a._1, (a._2 - avg) * (a._2 - avg))).values.reduce(_ + _)
    println(mm)

    0d
  }

  def reduceByKeyB[K, V](collection: Traversable[Tuple2[K, V]])(implicit num: Numeric[V]) = {
    import num._
    collection
      .groupBy(_._1)
      .map { case (group: K, traversable) => traversable.reduce { (a, b) => (a._1, a._2 + b._2) } }
  }


  def equalizeCounts(hist: Map[Int, Int], parts: Int): Map[Int, Int] = {

    val ALLOWED_BUFFER = 0.1 // 10% over the total/parts
    val sortedkm = hist.toSeq.sortBy(_._2)
    val total = hist.values.foldLeft(0)((m: Int, n: Int) => m + n)
    var maxPerPart: Int = ((total / parts) + ALLOWED_BUFFER * (total / parts)).toInt + 1
    val arr = sortedkm.toArray
    var begin = 0
    var end = arr.length - 1
    var listOfLists: ArrayBuffer[List[Int]] = new ArrayBuffer()

    breakable {
      for (i <- (arr.length - 1) to 0 by -1) {
        println("-------------" + i)
        var loopContinue = false
        var arrBuff = new ArrayBuffer[Int]()
        var count = arr(i)._2
        arrBuff += arr(i)._1

        if (count > maxPerPart) {
          maxPerPart = (((total - count) / parts - 1) + ALLOWED_BUFFER * ((total - count) / parts - 1)).toInt + 1
          loopContinue = true
          listOfLists = listOfLists += (arrBuff.toList)
        }

        if (loopContinue == false) {
          if (i == begin) {
            listOfLists = listOfLists += (arrBuff.toList)
            break
          }

          // println("pre  :" + arrBuff.length)
          count = innerloopFunc(count, arrBuff, i)
          //println("post :" + arrBuff.length)
        }

        if (i == begin) {
          break
        }
      }
    }


    def innerloopFunc(acount: Int, arrBuff: ArrayBuffer[Int], i: Int): Int = {
      var count = acount
      for (j <- begin to arr.length) {
        var breakInnerLoop = false

        count = count + arr(j)._2
        if (count <= maxPerPart) {
          arrBuff += arr(j)._1
          begin = begin + 1
        }
        if (count >= maxPerPart || begin == i) {
          listOfLists += (arrBuff.toList)
          return count
        }

        if (i == begin) return count

      }
      count
    }

    while (listOfLists.length > parts) {
      val atemp: List[Int] = listOfLists.remove(listOfLists.length - 1)
      val altemp: List[Int] = listOfLists.remove(listOfLists.length - 1)
      listOfLists += (atemp ++ altemp)

    }

    println(" sample provider Ids in part1 " + listOfLists(0))

    var count: Int = 0
    val m: Map[Int, Int] = listOfLists.flatMap(a => {
      val ll = a.map(t => (t, count))
      count = count + 1
      ll.toIterator
    }).toMap

    m
  }


  class ExactPartitioner[V](
                             partitions: Int,
                             keyMap: Map[Int, Int])
    extends org.apache.spark.Partitioner {

    def getPartition(key: Any): Int = {
      val k = key.asInstanceOf[Int]
      // `k` is assumed to go continuously from 0 to elements-1.
      return keyMap(k)
    }

    override def numPartitions: Int = partitions
  }


}