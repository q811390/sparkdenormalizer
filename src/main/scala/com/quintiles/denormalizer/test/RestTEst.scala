package com.quintiles.denormalizer.test

import java.net.URL

import com.stackmob.newman.ApacheHttpClient
import com.stackmob.newman.dsl._
import org.json4s.jackson.JsonMethods._

import scala.concurrent._
import scala.concurrent.duration._


/**
 * Created by dhiraj on 5/20/15.
 */
object RestTest {

  val fromHost = List("http://usadc-shdsxt04:7000/solr/shard0")
  val toHost = List(
    "http://usadc-shdsxt04:8983/solr/shard1",
    "http://usadc-shdsxt04:8983/solr/shard2",
    "http://usadc-shdsxt04:8983/solr/shard3",
    "http://usadc-shdsxt04:8983/solr/shard4",
    "http://usadc-shdsxt04:8983/solr/shard5",
    "http://usadc-shdsxt04:8983/solr/shard6",
    "http://usadc-shdsxt04:8983/solr/shard7",
    "http://usadc-shdsxt04:8983/solr/shard8",
    "http://usadc-shdsxt04:8983/solr/shard9",
    "http://usadc-shdsxt04:8983/solr/shard10",
    "http://usadc-shdsxt04:8983/solr/shard11",
    "http://usadc-shdsxt04:8983/solr/shard12")

  //val PP = List("Patients","Providers")


  val countVals = List("LAB", "DIAG", "VITAL", "MED")
  // val countConst = "/count?q=EventID%3A###*&start=0&rows=100&wt=json&indent=true"
  val countConst = "/select?q=EventID%3A###*&wt=json&indent=false"


  //http://usadc-shdsxt04:7000/solr/shard0/count?q=EventID%3A###*&start=0&rows=100&wt=json&indent=true

  def main(args: Array[String]) {


    countVals.foreach(a => {
      val modifiedCV = countConst.replace("###", a)

      val toSumTot: Int = toHost.map(a => getCount(a + modifiedCV, "numFound")).reduce(_ + _)

      val fromSumTot: Int = fromHost.map(a => getCount(a + modifiedCV, "numFound")).reduce(_ + _)

      println(" checking counts from both the sourcess and they are for " + a + "  from " + fromSumTot + "  to  " + toSumTot)
      assert(fromSumTot == toSumTot)
    })

  }


  def getCount(urlStr: String, countVal: String): Int = {
    implicit val httpClient = new ApacheHttpClient
    //execute a GET request
    val url = new URL(urlStr)
    val response = Await.result(GET(url).apply, 10.second) //this will throw if the response doesn't return within 1 second
    //    println(s"Response returned from ${url.toString} with code ${response.code}, body ${response.bodyString}")
    val json = response.bodyString

    val mt = compact(parse(json) \\ countVal)
    mt.toInt

  }


}
