import java.io._
import java.util.Properties

import com.quntiles.vantage.denormalizer._
import com.quntiles.vantage.denormalizer.contracts._
import com.quntiles.vantage.denormalizer.records.PatientRecord
import com.quntiles.vantage.denormalizer.vo.ConstantEvents
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.solr.client.solrj.embedded.EmbeddedSolrServer
import org.apache.solr.common.SolrInputDocument
import org.apache.solr.core.CoreContainer
import org.apache.solr.core.CoreContainer.Initializer
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{Logging, SparkConf, SparkContext}

import scala.collection.JavaConversions._
import scala.collection.mutable.ArrayBuffer

/**
 * Created by q811390 on 4/20/2015.
 */

case class IndexRow(patientId: Int, patientRecord: PatientRecord)

object PullSqlServerData extends Logging {

  // val logger = Logger(LoggerFactory.getLogger(this))


  val medicationsFile = "C:\\Users\\q811390\\Desktop\\solr_input_files_06_2015_compress\\medications.40.txt"
  val providersFile = "C:\\Users\\q811390\\Desktop\\solr_input_files_06_2015_compress\\providers.40.txt"
  val icd9Path = "C:\\Users\\q811390\\Desktop\\solr_input_files_06_2015_compress\\icd9strings.txt"
  val medicationDosageForm = "C:\\Users\\q811390\\Desktop\\solr_input_files_06_2015_compress\\MedicationDosageForm.txt"
  //val data_folder = "C:\\Users\\q811390\\Documents\\rawData\\EventStoreExport.solrgen0.txt"
  val data_folder = "C:\\Users\\q811390\\Desktop\\solr_input_files_06_2015_compress\\EventStoreExport_small.txt"
  val solrOutput = "C:\\Users\\q811390\\Desktop\\solr_input_files_06_2015_compress\\output\\"
  val solrSchema = "C:\\Users\\q811390\\Quintiles_proj\\DataDenormalizer\\solr\\solr.xml"
  var mainPartitions = 5

  def main(args: Array[String]) {


    val configFileProperties: Properties = if (args.length > 0) {

      val t = new Properties();
      t.load(new BufferedReader(new FileReader(args(0))));
      mainPartitions = if (args.length == 2) Integer.parseInt(args(1)) else 5
      t
    } else null


    val user = configFileProperties.getOrElse("USER", "")
    val password = configFileProperties.getOrElse("PASSWORD", "")
    val jdbcUrl = configFileProperties.getOrElse("JDBC_URL", "")
    val schema = configFileProperties.getOrElse("SCHEMA", "")


    val mFile = configFileProperties.getOrElse("MEDICATIONS_FILE", medicationsFile) //if (configFileProperties != null) configFileProperties.getProperty("MEDICATIONS_FILE") else medicationsFile
    val pFile = configFileProperties.getOrElse("PROVIDERS_FILE", providersFile)
    val icd9File = configFileProperties.getOrElse("ICD_FILE", icd9Path)
    val mdFile = configFileProperties.getOrElse("MEDICATIONS_DOSAGE_FILE", medicationDosageForm)
    val eventsDataFolder = configFileProperties.getOrElse("EVENTS_FOLDER", data_folder)
    val solrOutputFolder = configFileProperties.getOrElse("SOLR_OUTPUT", solrOutput)
    val solrSchemaFile = configFileProperties.getOrElse("SOLR_SCHEMA", solrSchema)
    val hdfsPath = configFileProperties.getOrElse("HDFS_PATH", null)
    val local = configFileProperties.getOrElse("LOCAL", null)
    val configJar = configFileProperties.getOrElse("CONFIG_JAR", "hdfs:///user/webadm/solr_input_files_06_2015/conf.jar")
    val listFilesProviders: String = configFileProperties.getOrElse("FILES_LIST", null)

    val arrSeq = Seq(classOf[OutPatients], classOf[VitalsInfo], classOf[Event], classOf[LabInfo], classOf[PatientRecord], classOf[PatientInfo], classOf[ConstantEvents], classOf[DosageFormsFilterList], classOf[ICD9], classOf[Medications], classOf[ProviderData], classOf[DiagnosisInfo]).toArray
    val conf = if (configFileProperties == null || local != null) new SparkConf()
      .setMaster("local[1]")
      .setAppName("SparkDeserializer")
      .set("spark.driver.memory", "13g")
    else new SparkConf()
    conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    conf.registerKryoClasses(arrSeq.toArray)

    val sc = new SparkContext(conf)
    //sbt sc.addJar(configJar)

    val medicationsRdd: RDD[String] = sc.textFile(mFile)
    val medications = sc.broadcast(new Medications(null, medicationsRdd.collect().toSeq))

    val providerRdd: RDD[String] = sc.textFile(pFile)
    val mmTempProviders = new ProviderData(null, providerRdd.collect().toSeq)
    val providers = sc.broadcast(mmTempProviders)


    val icd9Rdd: RDD[String] = sc.textFile(icd9File)
    val icd9 = sc.broadcast(new ICD9(null, icd9Rdd.collect().toSeq))

    val dosageRdd: RDD[String] = sc.textFile(mdFile)
    val dosageFormsFilterList = sc.broadcast(new DosageFormsFilterList(null, dosageRdd.collect().toSeq))


    val patientsMap = if (listFilesProviders != null) getPatientIdsMap(sc, listFilesProviders) else null

    val temp: RDD[(Int, Event)] = sc.textFile(eventsDataFolder)
      .map(a => ({
        val v = new Event(a)
        (v.getValue(3).toInt, v)
      }))

    val numPart = temp.partitions.length

    logWarning("number of partitions temp :" + temp.partitions.length)
    val nevents: RDD[(Int, Event)] = temp.sortByKey(true, (numPart * 2)).persist(StorageLevel.DISK_ONLY)
    logWarning("number of partitions nevents : " + nevents.partitions.length)


    val key: RDD[(Int, PatientRecord)] = nevents.mapPartitions(a => {

      var patientId: Int = 0
      var patientRecord: PatientRecord = null
      var patientRecords = ArrayBuffer[(Int, PatientRecord)]()

      Indexer.initialize(new ConstantEvents(medications.value, providers.value, dosageFormsFilterList.value, icd9.value))
      var count: Long = 0
      a.foreach(b => {

        if (b._1 != patientId) {
          if (patientRecord == null) {
            patientRecord = new PatientRecord(true)
          } else {

            if (count % 100 == 0) {
              logWarning(" count :: " + count + " this " + this + "  time " + System.currentTimeMillis() + "  free memory  " + Runtime.getRuntime.freeMemory())
            }
            count = count + 1
            patientRecords.add((patientRecord.getPatient.getPatientId, patientRecord))
            patientRecord = new PatientRecord(true)
          }
          patientId = b._1
        }
        patientRecord.addEvent(b._2)

      })

      if (patientRecord != null && patientRecord.getPatient != null && patientRecord.getPatient.getPatientId != null)
        patientRecords.add((patientRecord.getPatient.getPatientId, patientRecord))


      patientRecords.toIterator
    }).persist(StorageLevel.DISK_ONLY)
    var index = 0

    val totalPatients = key.count().toInt //map(a => a.vitalsInfos.length).sum()

    nevents.unpersist()
    print("totalVitals  : " + totalPatients)
    // patientRecords.unpersist(true)

    writeToIndex(key, totalPatients)

    System.out.println(" All done End indexing....  ")
    sc.stop()


    def writeToIndex(patientRecords: RDD[(Int, PatientRecord)], totalPatients: Int): Unit = {

      val sqlContext = new org.apache.spark.sql.SQLContext(sc)

      print("Begine writing to index")
      //      val hist: Map[Int, Int] = patientRecords.keys.countByValue().map(a => (a._1, a._2.toInt)).toMap
      // val partsMap = sc.broadcast(patientsMap)
      //
      val persistNew: RDD[(Int, PatientRecord)] = patientRecords.map(a => (a._2.getPatient.getProviderId, a)).repartition(mainPartitions).values //.partitionBy(new ExactPartitioner(mainPartitions, partsMap.value))
      //      val listAllParts = persistNew.mapPartitionsWithIndex((index, it) => {
      //        Iterator(index)
      //      }).collect()


      val am = persistNew.mapPartitions(i => {
        Iterator(i.size)
      }).collect().toList

      println(" The data is distributed " + am)

      val persist = persistNew.map(a => (IndexRow(a._1, a._2)))

      //.toDF("finalRDDtoIndex")
      //      toIndexPersist.setName("finalRDDtoIndex").saveAsObjectFile(hdfsPath + "/toIndex.seq")


      //  println(" partition Index are  " + listAllParts.toList)

      //      val persist: RDD[IndexRow] = sc.objectFile(hdfsPath + "/toIndex.seq").setName("finalRDDtoIndexFromSeqFile")
      print("num pertitions are :: " + persist.partitions.length)
      print("Starting writing to disk.... free memory " + Runtime.getRuntime().freeMemory())

      // persist.select("patientId").saveAsParquetFile(hdfsPath + "/patientsInfo.parquet")
      // persist.count()
      // patientRecords.unpersist(true)
      println("about to write to Index")

      val finalRes = persist.mapPartitionsWithIndex((index, it) => {
        val initializer: Initializer = new Initializer
        val container: CoreContainer = initializer.initialize
        container.load(solrOutputFolder + "/solr", new File(solrSchemaFile))

        val aIndex = index + 1
        val solrServer = new EmbeddedSolrServer(container, "shard" + aIndex)

        var eventsCount = 0
        var counter: Int = 0
        it.foreach(nrow => {

          //val nrow = row.asInstanceOf[IndexRow]

          if (counter % 100 == 0) {
            val providerId = if (nrow.patientRecord != null) nrow.patientRecord.getPatient.getProviderId else null
            logWarning(" time : " + System.currentTimeMillis() + "  processed " + counter + " out of " + totalPatients + " free memory " + Runtime.getRuntime().freeMemory() + " processed events " + eventsCount + " provider id " + providerId)
            eventsCount = 0
          }
          counter = counter + 1
          val de: PatientRecord = nrow.patientRecord
          val events: List[OutputPatient] = de.getSchemaLines.toList

          eventsCount = eventsCount + events.size
          var tnum = 0
          events.foreach(event => {
            val doc: SolrInputDocument = event.getDoc
            solrServer.add(doc)

            tnum = tnum + 1
            if (tnum % 1000 == 0)
              solrServer.commit()
          })
          solrServer.commit()
        })

        solrServer.commit()
        solrServer.optimize()
        solrServer.shutdown()
        if (hdfsPath != null) {
          val p = new Path(hdfsPath)
          val fs = FileSystem.get(new Configuration())
          fs.moveFromLocalFile(new Path(solrOutputFolder + "/solr/shard" + aIndex), new Path(hdfsPath))
          fs.close()

        }
        List().iterator
      })
      finalRes.count()
      print("End writing to index ")
    }
  }

  def getPatientIdsMap(sc: SparkContext, listFilesProviders: String): Map[Int, Int] = {
    val files = sc.textFile(listFilesProviders).collect()
    var counter = -1
    val listPatientIds = files.map(file => {
      counter = counter + 1

      val m = sc.textFile(file).map(a => a.split("\\|")(3).toInt).distinct().collect().map(a => (a, counter)).toMap
      println(s" the index is  $counter  and the number of patients are " + m.size)
      m

    })

    listPatientIds.reduce(_ ++ _)
  }

  class ExactPartitioner[V](
                             partitions: Int,
                             keyMap: Map[Int, Int])
    extends org.apache.spark.Partitioner {

    def getPartition(key: Any): Int = {
      val k = key.asInstanceOf[Int]
      // `k` is assumed to go continuously from 0 to elements-1.
      return keyMap(k)
    }

    override def numPartitions: Int = partitions
  }

}
