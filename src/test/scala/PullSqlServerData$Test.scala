import com.quintiles.denormalizer.EqualPartitioner.ExactPartitioner
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.FunSuite

/**
 * Created by q811390 on 7/17/2015.
 */
class PullSqlServerData$Test extends FunSuite {

  test("testData_folder") {

    val conf = new SparkConf().setAppName("HelloWorld").setMaster("local")
    val sc = new SparkContext(conf)

    val m: Map[Int, Int] = Map(1 -> 0, 2 -> 0, 3 -> 1, 4 -> 1, 5 -> 2, 6 -> 2, 7 -> 2, 8 -> 2, 9 -> 2, 10 -> 2)
    val mb = sc.broadcast(m)
    val toList: List[Int] = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    val rdd: RDD[(Int, Int)] = sc.parallelize(toList).map(a => (a, a)).repartition(3)
    val nrdd = rdd.partitionBy(new ExactPartitioner(3, mb.value))

    val collect = nrdd.glom().collect()
    var n = 0
    val mm = collect.flatMap(a => {
      val m = a.map(a => (a._1, n));
      n = n + 1;
      m
    }).toMap
    //assert(collect.foreach())
    println("the list is " + m)
    println("the list is " + mm)
    assert(m == mm)
    sc.stop()
  }

}
