import sbt.Keys._
import sbt._


name := "SparkDenormalizer"

version := "1.0"

scalaVersion := "2.10.4"

scalacOptions += "-target:jvm-1.7"

javacOptions ++= Seq("-source", "1.7", "-target", "1.7")

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.4.1" % "provided"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "1.4.1" % "provided"

libraryDependencies += "org.apache.solr" % "solr-core" % "4.3.0"

libraryDependencies += "com.esotericsoftware.kryo" % "kryo" % "2.24.0"

libraryDependencies += "com.stackmob" %% "newman" % "1.3.5"

libraryDependencies += "joda-time" % "joda-time" % "2.7"

libraryDependencies += "org.json4s" %% "json4s-native" % "3.2.11"

resolvers += "justwrote" at "http://repo.justwrote.it/releases/"

resolvers += "Restlet Repository" at "http://maven.restlet.org"

libraryDependencies += "it.justwrote" %% "scala-faker" % "0.3"

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "3.0.0-SNAP5" % "test"

assemblyJarName in assembly := "spark_denormalizer_v1.jar"